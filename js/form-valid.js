//form validation


function validateForm() {
			var user_name = document.comment_form.user_name;
			var email     = document.comment_form.email;
			var comment   = document.comment_form.comment;
			var subject   = document.comment_form.subject;

			if (user_name.value == '') {
				document.getElementById('show-tm').innerHTML = 'Please Enter Your Name...';

				return false;
			}

			if (email.value == '') {
				document.getElementById('show-tm').innerHTML = 'Please Enter Email...';

				return false;
			}

			if (email.value.indexOf('@', 0) < 0) {
				document.getElementById('show-tm').innerHTML = 'Please Enter a Valid Email...';

				return false;
			}

			if (email.value.indexOf('.', 0) < 0) {
				document.getElementById('show-tm').innerHTML = 'Please Enter a Valid Email...';

				return false;
			}

			if (subject.value == '') {
				document.getElementById('show-tm').innerHTML = 'Please Enter Subject...';

				return false;
			}
			
			if (comment.value == '') {
				document.getElementById('show-tm').innerHTML = 'Please Leave With a Comment...';

				return false;
			}
		}